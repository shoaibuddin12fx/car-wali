import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SearchService {
  constructor() {}
  term;
  option;

  search(term, option) {
    console.log('Search: ', term, ' option:', option);
    this.term = term;
    this.option = option;
  }
}
