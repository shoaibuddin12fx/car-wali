import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from 'src/app/base-page/base-page';

@Component({
  selector: 'app-add-car',
  templateUrl: './add-car.page.html',
  styleUrls: ['./add-car.page.scss'],
})
export class AddCarPage extends BasePage implements OnInit {
  aForm: FormGroup;
  loading = false;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.setupForm();
  }

  close() {
    this.modals.dismiss({ data: 'A' });
  }

  setupForm() {
    this.aForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
      cnic: ['', Validators.compose([Validators.required])],
      phone: ['', Validators.compose([Validators.required])],
      address: ['', Validators.compose([Validators.required])],
      status: ['', Validators.compose([Validators.required])],
    });
  }

  async save() {
    console.log(this.aForm.value);
    if (!this.aForm.valid) {
      if (!this.aForm.controls['name'].valid) {
        this.utility.presentFailureToast('Please fill name');
        return;
      }
      if (!this.aForm.controls['cnic'].valid) {
        this.utility.presentFailureToast('Please fill cnic');
        return;
      }
      if (!this.aForm.controls['phone'].valid) {
        this.utility.presentFailureToast('Please fill phone');
        return;
      }
      if (!this.aForm.controls['address'].valid) {
        this.utility.presentFailureToast('Please fill address');
        return;
      }
      if (!this.aForm.controls['status'].valid) {
        this.utility.presentFailureToast('Please fill status');
        return;
      }
      // if (!this.aForm.controls['year'].valid) {
      //   this.utility.presentFailureToast('Please select date');
      //   return;
      // }

      this.utility.presentFailureToast('Please fill out the form');
      return;
    }

    var formdata = this.aForm.value;
    console.log();

    const res = await this.sqlite.setCar(formdata);
    console.log(res);

    this.aForm.reset();

    // this.modals.dismiss({ reload: true });
  }
}
