import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from 'src/app/base-page/base-page';
import { AddCarPage } from '../add-car/add-car.page';
import { SearchPage } from '../search/search.page';
import { SettingsPage } from '../settings/settings.page';
import { Share } from '@capacitor/share';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.page.html',
  styleUrls: ['./vehicles.page.scss'],
})
export class VehiclesPage extends BasePage implements OnInit {
  list = [];
  term = '';
  option = 'cnic';
  isSearchOpen = false;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}

  ionViewDidEnter() {
    this.getCarList();
  }

  async addCar() {
    const res = await this.modals.present(AddCarPage, null);
    this.getCarList();
  }

  async getCarList() {
    this.initValues();
    const res = await this.sqlite.getAllCars(this.term, this.option);
    console.log(res);
    this.list = res as [];
    this.list.push({
      name: 'Abdullah',
      address: 'test',
      cnic: 1234567891234,
      status: 'Activ',
      phone: 123,
    });
  }

  async delete(item) {
    const res = await this.sqlite.deleteCar(item.cnic);
    this.getCarList();
  }

  async gotoSettings() {
    //
    const res = await this.modals.present(SettingsPage);
    this.getCarList();
  }

  searchUpdate() {
    this.getCarList();
  }

  goBack() {
    this.nav.pop();
  }

  async showSearch() {
    const res = await this.modals.present(SearchPage, null);
    console.log(res);
    this.getCarList();
  }

  initValues() {
    this.term = this.searchService.term;
    this.option = this.searchService.option;
  }

  async share(item) {
    await Share.share({
      title: 'CardDb',
      text: this.getShareText(item),
      //  url: 'http://ionicframework.com/',
      dialogTitle: 'Share with others',
    });
  }

  getShareText(item) {
    return `Name=${item.name}\nCNIC1=${item.cnic}\nPhone=${item.phone}\nAddress=${item.address}`;
  }
}
