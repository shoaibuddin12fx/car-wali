import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from 'src/app/base-page/base-page';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss'],
})
export class SplashPage extends BasePage implements OnInit {
  loading = false;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}

  ionViewDidEnter() {
    this.initialize();
  }

  async initialize() {
    this.loading = true;
    const res = await this.sqlite.initialize();
    this.loading = false;
  }

  gotohome() {
    this.nav.push('pages/home');
  }
}
