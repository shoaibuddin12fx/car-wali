import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Chooser } from '@awesome-cordova-plugins/chooser/ngx';
import { BasePage } from 'src/app/base-page/base-page';
import { AddCarPage } from '../add-car/add-car.page';
import { SearchPage } from '../search/search.page';
import { DomSanitizer } from '@angular/platform-browser';
import { Directory, Encoding, Filesystem } from '@capacitor/filesystem';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage extends BasePage implements OnInit {
  aForm: FormGroup;
  loading = false;
  showPopover = false;
  blob;

  constructor(injector: Injector, private chooser: Chooser) {
    super(injector);
    this.presentListPage = this.presentListPage.bind(this);
  }

  ngOnInit() {
    this.setupForm();
  }

  async addCar() {
    const res = await this.modals.present(AddCarPage, null);
  }

  // closeSearch() {
  //   console.log('closing');
  //   this.isSearchOpen = false;
  //   setTimeout(this.presentListPage, 100);
  // }

  async searchUser() {
    const res = await this.modals.present(SearchPage, null);
    console.log(res);
  }

  async presentListPage() {
    this.nav.push('pages/vehicles');
  }

  showHideMenu(val) {
    this.showPopover = val;
  }

  showMenu() {
    this.showPopover = true;
  }

  setupForm() {
    this.aForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
      cnic: ['', Validators.compose([Validators.required])],
      phone: ['', Validators.compose([Validators.required])],
      address: ['', Validators.compose([Validators.required])],
      status: ['', Validators.compose([Validators.required])],
    });
  }

  async save() {
    console.log(this.aForm.value);
    if (!this.aForm.valid) {
      if (!this.aForm.controls['name'].valid) {
        this.utility.presentFailureToast('Please fill name');
        return;
      }
      if (!this.aForm.controls['cnic'].valid) {
        this.utility.presentFailureToast('Please fill cnic');
        return;
      }
      if (!this.aForm.controls['phone'].valid) {
        this.utility.presentFailureToast('Please fill phone');
        return;
      }
      if (!this.aForm.controls['address'].valid) {
        this.utility.presentFailureToast('Please fill address');
        return;
      }
      if (!this.aForm.controls['status'].valid) {
        this.utility.presentFailureToast('Please fill status');
        return;
      }

      this.utility.presentFailureToast('Please fill out the form');
      return;
    }

    var formdata = this.aForm.value;
    console.log();

    const res = await this.sqlite.setCar(formdata);
    console.log(res);
    this.utility.presentFailureToast('Record added successfully!');
    this.aForm.reset();

    //this.modals.dismiss({ reload: true });
  }

  async exportData() {
    this.showHideMenu(false);
    var table = [];

    const res = (await this.sqlite.getAllCars()) as [];
    console.log(res);

    if (res.length == 0) {
      this.utility.presentToast('Please add record in app first');
      return;
    }

    table = [...table, ...res];

    await Filesystem.writeFile({
      path: 'cardb.txt',
      data: JSON.stringify(table),
      directory: Directory.Documents,
      encoding: Encoding.UTF8,
    });

    this.utility.presentToast('File saved as cardb.txt');
  }

  async importData() {
    this.showHideMenu(false);
    this.chooser
      .getFile('text/plain')
      .then(async (file) => {
        if (file) {
          console.log(file ? file.name : 'canceled');
          console.log(file.data);

          var line = new TextDecoder('utf-8').decode(file.data);
          var json = JSON.parse(line);
          console.log(json);

          if (json.length == 0) {
            return;
          }

          console.log('json', json);

          await this.sqlite.setCars(json);
          this.utility.presentToast('Data imported successfully!');
          this.nav.push('pages/vehicles');
        }
      })
      .catch((error: any) => console.error(error));
    // }
  }
}
