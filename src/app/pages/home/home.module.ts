import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';

import { HomePage } from './home.page';
import { AddCarPageModule } from '../add-car/add-car.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SearchComponentModule } from 'src/app/components/search/search.module';
import { SettingsComponentModule } from 'src/app/components/settings/settings.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    AddCarPageModule,
    Ng2SearchPipeModule,
    SearchComponentModule,
    AddCarPageModule,
    ReactiveFormsModule,
    SettingsComponentModule,
  ],
  declarations: [HomePage],
})
export class HomePageModule {}
