import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from 'src/app/base-page/base-page';
import { AddCarPage } from '../add-car/add-car.page';
import { VehiclesPage } from '../vehicles/vehicles.page';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage extends BasePage implements OnInit {
  option = 'cnic';
  term = '';
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}

  async search() {
    await this.modals.dismiss();
    this.searchService.search(this.term, this.option);
    this.presentListPage();
  }

  async presentListPage() {
    this.nav.push('pages/vehicles');
  }

  close() {
    this.modals.dismiss({
      test: 'A',
    });
  }
}
