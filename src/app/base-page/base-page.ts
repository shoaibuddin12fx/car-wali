import { Injector } from '@angular/core';
import { Location } from '@angular/common';
import { Platform, MenuController } from '@ionic/angular';
import { FormBuilder } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { SqliteService } from '../services/sqlite.service';
import { EventsService } from '../services/basic/events.service';
import { ModalService } from '../services/basic/modal.service';
import { NavService } from '../services/basic/nav.service';
import { StorageService } from '../services/basic/storage.service';
import { DataService } from '../services/data.service';
import { NetworkService } from '../services/network.service';
import { UserService } from '../services/user.service';
import { UtilityService } from '../services/utility.service';
import { SearchService } from '../services/serach.service';

export abstract class BasePage {
  public platform: Platform;
  public formBuilder: FormBuilder;
  public menuCtrl: MenuController;
  public domSanitizer: DomSanitizer;
  public modals: ModalService;
  public nav: NavService;
  public sqlite: SqliteService;
  public utility: UtilityService;
  public events: EventsService;
  public network: NetworkService;
  public storage: StorageService;
  public user: UserService;
  public dataService: DataService;
  public searchService: SearchService;

  constructor(injector: Injector) {
    this.platform = injector.get(Platform);
    this.formBuilder = injector.get(FormBuilder);
    this.menuCtrl = injector.get(MenuController);
    this.domSanitizer = injector.get(DomSanitizer);
    this.nav = injector.get(NavService);
    this.modals = injector.get(ModalService);
    this.utility = injector.get(UtilityService);
    this.events = injector.get(EventsService);
    this.network = injector.get(NetworkService);
    this.user = injector.get(UserService);
    this.storage = injector.get(StorageService);
    this.dataService = injector.get(DataService);
    this.sqlite = injector.get(SqliteService);
    this.searchService = injector.get(SearchService);
  }
}
