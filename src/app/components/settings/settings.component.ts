import { Component, Injector, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Chooser } from '@awesome-cordova-plugins/chooser/ngx';
import { Directory, Encoding, Filesystem } from '@capacitor/filesystem';
import { BasePage } from 'src/app/base-page/base-page';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent extends BasePage implements OnInit {
  loading = false;
  blob;

  constructor(
    injector: Injector,
    private chooser: Chooser,
    private sanitizer: DomSanitizer
  ) {
    super(injector);
  }

  ngOnInit() {}

  close() {
    this.modals.dismiss({ data: 'A' });
  }

  async exportData() {
    var table = [];

    const res = (await this.sqlite.getAllCars()) as [];
    console.log(res);

    if (res.length == 0) {
      this.utility.presentToast('Please add record in app first');
      return;
    }

    table = [...table, ...res];

    // for (var i = 0; i < 26; i++) {
    //   var obj = {
    //     id: i,
    //     square: i * i,
    //   };
    //   table.push(obj);
    // }

    // this.blob = new Blob(table, { type: 'data:text/json;charset=utf-8' });
    // this.writeFile();
    // this.file
    //   .checkDir(this.file.dataDirectory, 'mydir')
    //   .then((_) => {
    //     console.log(this.file.dataDirectory, 'Directory exists');
    //   })
    //   .catch((err) => {
    //     console.log(Directory.Documents, 'Directory doesnt exist');
    //   });
    // // this.

    await Filesystem.writeFile({
      path: 'cardb.txt',
      data: JSON.stringify(table),
      directory: Directory.Documents,
      encoding: Encoding.UTF8,
    });

    this.utility.presentToast('File saved as cardb.txt');
    this.modals.dismiss({ data: 'A' });

    //Check the Browser.

    // var url = window.URL || window.webkitURL;
    // var link = url.createObjectURL(blob1);
    // var a = document.createElement('a');
    // a.download = 'Customers.txt';
    // a.href = link;
    // document.body.appendChild(a);
    // a.click();
    // document.body.removeChild(a);
  }

  async importData() {
    // const res = await Filesystem.getUri({
    //   path: 'cardb.txt',
    //   directory: Directory.Documents,
    // });

    // console.log(res);
    // let uri = res.uri ? res.uri : null;
    // if (uri) {
    //   const file = await Filesystem.readFile({
    //     path: 'cardb.txt',
    //     directory: Directory.Documents,
    //   });

    //   console.log(file);
    this.chooser
      .getFile('text/plain')
      .then((file) => {
        if (file) {
          console.log(file ? file.name : 'canceled');
          console.log(file.data);

          var line = new TextDecoder('utf-8').decode(file.data);
          var json = JSON.parse(line);
          console.log(json);

          if (json.length == 0) {
            return;
          }

          console.log('json', json);

          this.sqlite.setCars(json);
          this.modals.dismiss({ data: json });
        }
      })
      .catch((error: any) => console.error(error));
    // }
  }
}
