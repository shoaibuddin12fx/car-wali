import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from 'src/app/base-page/base-page';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent extends BasePage implements OnInit {
  option = 'cnic';
  term = '';
  @Input() refEvent;
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}

  onSearch(isSearch) {
    this.refEvent();
    if (isSearch) this.searchService.search(this.term, this.option);
  }
}
