import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.waaps.cardb',
  appName: 'CarDB',
  webDir: 'www',
  bundledWebRuntime: false,
};

export default config;
